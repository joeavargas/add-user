# Add User

[Live Demo](http://www.joevargas.io/AddUser/index.html)

Simple little JS app that grabs user and email string from a textfields and adds them to a `ul` list upon clicking(click event) the Submit button below. Please note all info entered is saved to local storage so the data entered will be deleted when the pages refreshes or the browser is closed.

Putting this little project together helps me understand how JS and HTMl can work together when it comes to event handling. For instance, when clicking on the Submit button, I'm able to apply some form verification by making sure there is a name and email typed in. If not, I can trigger an error message to the user to make sure they enter the necessary fields. When the form verification is satisfied, it moves on to append the username and email to the `ul` below. 

![](/demo/addUserDemo.gif)