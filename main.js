// Cache the DOM
const myForm = document.querySelector('#my-form');
const nameInput = document.querySelector('#name');
const emailInput = document.querySelector('#email');
const msg = document.querySelector('.msg');
const userList = document.querySelector('#users');

// Listen for form submit
myForm.addEventListener('submit', onSubmit);

function onSubmit(e){
    e.preventDefault();

    //If Name and Email inputs are empty....
    if(nameInput.value === '' || emailInput.value === '') {
        //...alert the user
        msg.classList.add('error');
        msg.innerHTML = 'Please enter all fields';
        //remove alert message after 3 seconds
        setTimeout(() => msg.remove(), 3000);
    } else {
        //Create new list item with user
        const li = document.createElement('li');
        //Add text node with the input values
        li.appendChild(document.createTextNode(`${nameInput.value} : ${emailInput.value}`));
        //Append to li
        userList.appendChild(li);
        // Clear fields
        nameInput.value = '';
        emailInput.value = '';

    }
}